//Nama provinsi
$.ajax({
    // ajax url
    url: 'https://rs-bed-covid-api.vercel.app/api/get-provinces',
    dataType: 'json',
    type: 'GET',
    // Kalo datanya berhasil diambil, jalanin function success
    success: function(response) {                           // response itu data yang di return dari API
        const provinces = response.provinces;               //ambil nama provinsi
        let selectBox = document.getElementById('format');  //buat selectbox isinya provinsi
        for (let i = 0; i < provinces.length; i++ ) {
            let option = document.createElement('option');  //nama provinsi jadi opsi di selectbox
            option.innerHTML = provinces[i].name;           //ambil nama semua provinsi
            option.value = provinces[i].id;                 //ambil id semua provinsi
            selectBox.appendChild(option);                  //tambah provinsi ke selectbox
        }
    }
});

//Nama kota
function getCity() {
    let selectBox = document.getElementById('format');
    var value = selectBox.value;
    $.ajax({
        //url nama kota, valuenya berupa id provinsi
        url: `https://rs-bed-covid-api.vercel.app/api/get-cities?provinceid=${value}`,
        dataType: 'json',
        type: 'GET',
        success: function(response) {
            const cities = response.cities;                         //ambil nama kota
            let selectBox2 = document.getElementById('format2');    //buat selectbox isinya kota
            selectBox2.innerHTML = "";                              //
            for (let i=0; i<cities.length; i++) {
                let city = document.createElement('option');        //nama kota jadi opsi di selectbox
                city.innerHTML = cities[i].name;                    //
                city.value = `${value},${cities[i].id}`;            //
                selectBox2.appendChild(city);                       //tambah kota ke selectbox
            }
        }
    });
}

// Simpen ke daftar RS
function saveRS(val) {
    alert("berhasil ditambahkan!")
    $.ajax({
        type: 'POST',
        url: '/daftar-rs/',
        data:{
            rs_id: val,
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(response){
            console.log(response)
        }
    });
}

//Nama rs
function getHospital() {
    let value = document.getElementById('format2').value;
    let provinceId = value.split(',')[0];                               //id provinsi di index 0
    let cityId = value.split(',')[1];                                   //id city di index 1
    $.ajax({
        // ajax url
        url: `https://rs-bed-covid-api.vercel.app/api/get-hospitals?provinceid=${provinceId}&cityid=${cityId}&type=1`,
        dataType: 'json',
        type: 'GET',
        success: function(response) {
            const hospital = response.hospitals;                       //ambil nama rumah sakit
            const colors = ['warning', 'info', 'success', 'danger'];
            let i = 0;
            hospital.forEach(function(hospital) {
                //loop for each dari 0, buat setiap warna jadi left bordernya suatu card
                //tambah card ke webpage
                //kalau udah sampe warna danger reset ke 0
                let val = hospital.id;
                let card = `<div class="card border-left-${colors[i]} shadow h-100 py-2" style="width: 18rem; align-items: center;">
                <div class="card-body">
                    <h5 class="card-title text-${colors[i]}">${hospital.name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.address}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.phone}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">Kamar Tersedia: ${hospital.bed_availability}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.info}</h6>
                    </div>
                    <form>
                    <button type="button" style="border:none; outline:none;" onclick="saveRS(${val})">
                    <i class="fa fa-bookmark" aria-hidden="true" title="Simpan ke Daftar RS"></i>
                    </button>
                    </div></form>
                </div>`;
                $('#div').append(card);
                i++;
                if (i == 4) {
                    i = 0;
                }
            })
        }
    });
}
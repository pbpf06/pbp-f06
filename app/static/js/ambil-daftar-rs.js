$( document ).ready(function() {
    let value = document.getElementById('rs_id').innerHTML;
    let ids = value.split(" ");
    ids.forEach(function(id) {
        $.ajax({
            // ajax url
            url: `https://rs-bed-covid-api.vercel.app/api/get-bed-detail?hospitalid=${id}&type=1`,
            dataType: 'json',
            type: 'GET',
            success: function(response) {
                let hospital = response.data;
                let hospitalBed = hospital.bedDetail[0].stats;
                let card = `
                <div class="card border-left-info shadow h-100 py-2" style="width: 18rem; align-items: center;">
                <div class="card-body">
                    <h5 class="card-title text-info">${hospital.name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.address}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">${hospital.phone}</h6>
                    <h6 class="card-subtitle mb-2 text-muted">Kamar Tersedia: ${hospitalBed.bed_available}</h6>
                    <div class="d-flex justify-content-end">
                </div>
                </div>`;
                $('#daftar-rs').append(card);
            }
        });
    });
});
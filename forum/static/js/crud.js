$( document ).ready(function() {
    let pk;

    $('.delete-link').click(function(e) {
        pk = $(e.target).prev().val();
        $('#pk').val(pk);
    });
    
    $('.reply').click(function(e) {
        let cancel = $(e.target).prev();
        if (cancel.hasClass('invisible')) {
            cancel.removeClass('invisible');
        } else {
            pk = $(e.target).val();
            let id = '#replyArea' + pk;
            if ($(id).val() != "") {
                $(e.target).attr('type', 'submit');
            } else {
                $(e.target).attr('disabled', 'disabled');
                let emptyArea = '#emptyArea' + pk;
                $(emptyArea).html("*Reply tidak boleh kosong.");
            }
        }
    });

    $('.cancel').click(function(e) {
        let replyButton = '#reply' + pk;
        $(e.target).addClass('invisible');
        $(replyButton).removeAttr('disabled');
    });

    $('.reply-area').on('input', function(e) {
        let replyButton = '#reply' + pk;
        let replyArea = '#replyArea' + pk;
        let emptyArea = '#emptyArea' + pk;
        if ($(replyArea).val() != "") {
            $(replyButton).removeAttr('disabled');
            $(emptyArea).html("");
        }
    });
});